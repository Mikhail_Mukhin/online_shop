module.exports = {
    css: {
        loaderOptions: {
            sass: {
                prependData: './style/style.sass;'
            }
        }
    }
};