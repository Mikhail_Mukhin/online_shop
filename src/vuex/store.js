import Vue from "vue";
import Vuex from "vuex";

import actions from "./actions/actions";
import mutations from "./mutations/mutations";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: [],
    cart: [],
    searchValue: "",
  },
  mutations,
  actions,
  getters: {
    PRODUCTS(state) {
      return state.products;
    },
    CART(state) {
      return state.cart;
    },
    SEARCH_VALUE(state) {
      return state.searchValue;
    },
  },
});
