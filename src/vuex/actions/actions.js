import axios from "axios";

export default {
  GET_PRODUCTS_FROM_API({ commit }) {
    return axios("http://localhost:3000/products", {
      method: "GET",
    })
      .then((products) => {
        commit("SET_PRODUCTS_TO_STATE", products.data);
        return products;
      })
      .catch((error) => {
        console.log(error);
        return error;
      });
  },
  ADD_ELEM_TO_CART({ commit }, product) {
    commit("SET_ELEM_TO_CART", product);
  },
  DELETE_ELEM_IN_CART({ commit }, index) {
    commit("REMOVE_ELEM_IN_CART", index);
  },
  INCREMENT_ELEM({ commit }, index) {
    commit("INCREMENT", index);
  },
  DECREMENT_ELEM({ commit }, index) {
    commit("DECREMENT", index);
  },
  GET_SEARCH_VALUE({ commit }, value) {
    commit("SET_SEARCH_VALUE", value);
  },
};
