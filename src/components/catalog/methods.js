import { mapActions } from "vuex";

export default {
  ...mapActions(["GET_PRODUCTS_FROM_API", "ADD_ELEM_TO_CART"]),
  addElemToCart(data) {
    this.ADD_ELEM_TO_CART(data);
  },
  optionSelect(option) {
    if (option) {
      this.selectedCategory = option.category;

      if (this.selectedCategory === "headphones") {
        this.headphonesIsVisible = true;
        this.phonesIsVisible = false;
      }
      if (this.selectedCategory === "phones") {
        this.headphonesIsVisible = false;
        this.phonesIsVisible = true;
      }
      if (this.selectedCategory === "all") {
        this.headphonesIsVisible = true;
        this.phonesIsVisible = true;
      }
    }
    let vm = this;

    this.sortedProducts = [...this.PRODUCTS];
    let filterHeadphones = this.sortedProducts[0].headphones.filter(
      (elem) =>
        elem.name.toUpperCase().includes(this.SEARCH_VALUE.toUpperCase()) &&
        elem.price >= vm.minPrice &&
        elem.price <= vm.maxPrice
    );

    let filterphones = this.sortedProducts[1].phones.filter(
      (elem) =>
        elem.name.toUpperCase().includes(this.SEARCH_VALUE.toUpperCase()) &&
        elem.price >= vm.minPrice &&
        elem.price <= vm.maxPrice
    );

    if (this.selectedSort === "sort by rating") {
      console.log("rating");
      filterHeadphones = filterHeadphones.sort((a, b) => a.rating - b.rating);
      filterphones = filterphones.sort((a, b) => a.rating - b.rating);
    }

    this.sortedProducts = [
      { headphones: [...filterHeadphones] },
      { phones: [...filterphones] },
    ];
  },
  optionSort(option) {
    this.selectedSort = option.sortOption;
  },
  sortByPrice() {
    let filterHeadphones = this.sortedProducts[0].headphones.sort(
      (a, b) => a.price - b.price
    );

    let filterphones = this.sortedProducts[1].phones.sort(
      (a, b) => a.price - b.price
    );
    this.sortedProducts = [
      { headphones: [...filterHeadphones] },
      { phones: [...filterphones] },
    ];
  },
  sortByRating() {
    let filterHeadphones = this.sortedProducts[0].headphones.sort(
      (a, b) => a.rating - b.rating
    );

    let filterphones = this.sortedProducts[1].phones.sort(
      (a, b) => a.rating - b.rating
    );
    this.sortedProducts = [
      { headphones: [...filterHeadphones] },
      { phones: [...filterphones] },
    ];
  },
  limitingValue() {
    if (this.minPrice > this.maxPrice) {
      let tempVal = this.maxPrice;
      this.maxPrice = this.minPrice;
      this.minPrice = tempVal;
    }
    this.optionSelect();
  },
  searchProductsByValue(value) {
    let vm = this;

    this.sortedProducts = [...this.PRODUCTS];
    let filterHeadphones = this.sortedProducts[0].headphones.filter(
      (elem) =>
        elem.name.toUpperCase().includes(this.SEARCH_VALUE.toUpperCase()) &&
        elem.price >= vm.minPrice &&
        elem.price <= vm.maxPrice
    );

    let filterphones = this.sortedProducts[1].phones.filter(
      (elem) =>
        elem.name.toUpperCase().includes(value.toUpperCase()) &&
        elem.price >= vm.minPrice &&
        elem.price <= vm.maxPrice
    );
    this.sortedProducts = [
      { headphones: [...filterHeadphones] },
      { phones: [...filterphones] },
    ];
  },
};
