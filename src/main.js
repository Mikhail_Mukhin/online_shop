import Vue from "vue";
import App from "./App.vue";
import "./assets/style/style.sass";
import { BootstrapVue } from "bootstrap-vue";
import "material-design-icons-iconfont";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import store from "./vuex/store";
import router from "./router/router";

Vue.use(BootstrapVue);

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  store,
  router,
}).$mount("#app");
