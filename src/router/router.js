import Vue from "vue";
import Router from "vue-router";
import shopCatalog from "../components/catalog/shop-catalog";
import shopCart from "../components/cart/shop-cart";
import shopHeadphonesCatalog from "../components/headphones&phones/shop-headphones-catalog";
import shopPhonesCatalog from "../components/headphones&phones/shop-phones-catalog";
import shopHomePage from "../components/homepage/shop-home-page";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "ShopHomePage",
      component: shopHomePage,
    },
    {
      path: "/Catalog",
      name: "Catalog",
      component: shopCatalog,
      props: true,
    },
    {
      path: "/Cart",
      name: "Cart",
      component: shopCart,
      props: true,
    },
    {
      path: "/Catalog/Headphones",
      name: "headphones",
      component: shopHeadphonesCatalog,
      props: true,
    },
    {
      path: "/Catalog/phones",
      name: "phone",
      component: shopPhonesCatalog,
      props: true,
    },
  ],
});
