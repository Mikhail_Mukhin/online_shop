# Online shop

**to display the catalog of goods, in the terminal you need to write "json-server --watch db.json" so that the JSON server watches the db.json file**

In the project there is a base of two groups of goods. The product can be filtered by price, rating, product groups. Tt is possible to see the total number of products and the number of product names when hovering over the cart. In the basket itself, you can increase / decrease the number of products, delete an item. The total amount of all goods is also considered.

 
Online store project. Project made according to this [link](https://www.youtube.com/watch?v=22W54aRGr3Q&list=PLb6TvuNosCJW_N3wqAUYsp7DvUzfyvbvB&index=1&ab_channel=GoFrontend) lesson. In the project, I added a product rating, a filter by price (ascending) and by rating. in the cart when changing the number of products after clicking on "-" with a quantity of 1, the item is now deleted. I have extended "products" JSON. Now consists of two product groups instead of the "product group" property in the product object.
Hovering over the cart now shows the total number of products and the number of product names. The project is also adapted for any resolution (starting from 320px)